resource "google_compute_instance" "this" {
  provider     = google
  name         = "gcp-tf-instance"
  //machine_type = "e2-medium"
  machine_type = "f1-micro"
  zone         = "us-east1-b"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }

  network_interface {
    network = "default"
    access_config {
      // Left blank to assign public IP
    }
  }

  metadata = {
    ssh-keys = "${var.username}:${file("~/.ssh/id_rsa.pub")}"
  }

  metadata_startup_script = "sudo apt-get update && sudo apt -y install apache2; echo '<!doctype html><html><body><h1>Hello if you see this than you have apache running!</h1></body></html>' | sudo tee /var/www/html/index.html"
  scheduling {
        preemptible = true
        automatic_restart = false
    }
  
}





 
resource "google_compute_instance" "beta_this" {
  provider     = google-beta
  name         = "gcp-tf-beta-instance"
  machine_type = "e2-medium"
  zone         = "us-east1-b"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }

  network_interface {
    network = "default"
    access_config {
      // Left blank to assign public IP
    }
  }

  metadata = {
    ssh-keys = "${var.username}:${file("~/.ssh/id_rsa.pub")}"
  }

  metadata_startup_script = "sudo apt-get update && sudo apt -y install apache2; echo '<!doctype html><html><body><h1>Hello if you see this than you have apache running!</h1></body></html>' | sudo tee /var/www/html/index.html"
  scheduling {
        preemptible = true
        automatic_restart = false
    }
}